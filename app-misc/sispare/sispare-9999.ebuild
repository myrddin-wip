# Copyright 1999-2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit git-r3 myrddin-wip

DESCRIPTION="A simple spaced repetition program"
HOMEPAGE="https://repo.or.cz/sispare.git"
EGIT_REPO_URI="https://repo.or.cz/sispare.git"

if [[ ${PV} == 9999 ]]
then
	:
else
	EGIT_COMMIT="v${PV}"
fi

LICENSE="ISC"
SLOT="0"
KEYWORDS="~amd64"
