# Copyright (c) 2017 S. Gilles <sgilles@math.umd.edu>
# Distributed under the terms of the ISC License

# @ECLASS myrddin-wip.eclass
# @MAINTAINER:
# S. Gilles <sgilles@math.umd.edu>
# @BLURB: Build myrddin projects using dev-lang/myrddin
# @DESCRIPTION:
# An eclass providing functions to build myrddin projects using
# dev-lang/myrddin

case "${EAPI:-0}" in
	0|1|2|3|4|5)
		die "Unsupported EAPI=${EAPI:-0} (too old) for ${ECLASS}"
		;;
	6|7|8)
		;;
	*)
		die "Unsupported EAPI=${EAPI} (unknown) for ${ECLASS}"
		;;
esac

EXPORT_FUNCTIONS src_compile src_test src_install

RDEPEND=""
DEPEND="dev-lang/myrddin:="

# @FUNCTION: embld
# @USAGE: [<args>...]
# @DESCRIPTION:
# Call mbld. This function dies if mbld fails.
embld() {
	debug-print-function ${FUNCNAME} "${@}"

	mbld "$@" || die "mbld failed"
}

# @FUNCTION: myrddin-wip_src_compile
# @DESCRIPTION:
# Compile project with mbld
myrddin-wip_src_compile() {
	debug-print-function ${FUNCNAME} "${@}"

	embld "$@"
}

# @FUNCTION: myrddin-wip_src_test
# @DESCRIPTION:
# Run unit tests.
myrddin-wip_src_test() {
	debug-print-function ${FUNCNAME} "${@}"

	embld test
}

# @FUNCTION: myrddin-wip_src_install
# @DESCRIPTION:
# Install via mbld
myrddin-wip_src_install() {
	debug-print-function ${FUNCNAME} "${@}"

	DESTDIR="${D}" embld install
}
