# Copyright 1999-2019 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit git-r3 myrddin-wip

DESCRIPTION="terminal handling library for myrddin"
HOMEPAGE="https://git.eigenstate.org/npnth/libtermdraw.git"
EGIT_REPO_URI="https://git.eigenstate.org/npnth/libtermdraw.git"
if [[ ${PV} == 9999 ]]
then
	:
else
	EGIT_COMMIT="v${PV}"
	KEYWORDS="~amd64"
fi

LICENSE="ISC"
SLOT="0"
