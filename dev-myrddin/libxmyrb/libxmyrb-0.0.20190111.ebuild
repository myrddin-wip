# Copyright 1999-2019 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit git-r3 myrddin-wip

DESCRIPTION="X library for myrddin"
HOMEPAGE="https://git.eigenstate.org/ori/libxmyrb.git"
EGIT_REPO_URI="https://git.eigenstate.org/ori/libxmyrb.git"
EGIT_COMMIT="4212e61603d2a270ab839bd00c9a47a5ee241913"
KEYWORDS="~amd64"

LICENSE="ISC"
SLOT="0"

PATCHES=(
	"${FILESDIR}/0001-Read-HOME-from-environment.patch"
	"${FILESDIR}/0002-Specify-python2-in-bld.proj.patch"
	"${FILESDIR}/0003-Check-auth-addr-before-connecting.patch"
)
