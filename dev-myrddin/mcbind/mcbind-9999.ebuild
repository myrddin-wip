# Copyright 1999-2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit git-r3 myrddin-wip

DESCRIPTION="Automatic C binding generation for Myrddin"
HOMEPAGE="https://eigenstate.org/notes/mcbind.html"
EGIT_REPO_URI="git://git.eigenstate.org/ori/mcbind.git"
if [[ ${PV} == 9999 ]]
then
	:
else
	EGIT_COMMIT="7d539a7c08aba3f31b3913e0efef11c43ea9f9ef"
	KEYWORDS="~amd64"
fi

LICENSE="ISC" # I think it's implied...
SLOT="0"

src_prepare() {
	default
	chmod +x gen-defs.sh
	chmod +x gen-paths.sh
}
