# Copyright 2016-2019 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit git-r3 myrddin-wip

DESCRIPTION="Simple Markov chain(-esque) library for myrddin"
HOMEPAGE="http://www.math.umd.edu/~sgilles"
EGIT_REPO_URI="https://repo.or.cz/libmarkov.git"
if [[ ${PV} == 9999 ]]
then
	:
else
	EGIT_COMMIT="v${PV}"
	KEYWORDS="~amd64"
fi

LICENSE="ISC"
SLOT="0"
