# Copyright 1999-2019 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit git-r3 myrddin-wip

DESCRIPTION="A library for manipulating cluster algebras as quivers in myrddin"
HOMEPAGE="http://www.math.umd.edu/~sgilles"
EGIT_REPO_URI="https://repo.or.cz/libquiver.git"
if [[ ${PV} == 9999 ]]
then
	:
else
	EGIT_COMMIT="v${PV}"
	KEYWORDS="~amd64"
fi

RDEPEND="dev-myrddin/libt dev-myrddin/libyakmo"

LICENSE="ISC"
SLOT="0"
