# Copyright 2016-2019 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit git-r3

DESCRIPTION="orib's language"
HOMEPAGE="http://eigenstate.org/myrddin/"
EGIT_REPO_URI="https://git.eigenstate.org/ori/mc.git"
if [[ ${PV} == 9999 ]]
then
	KEYWORDS="~amd64"
else
	EGIT_COMMIT="r${PV}"
	KEYWORDS="amd64"
fi

LICENSE="ISC"
SLOT="0"
IUSE="static"

DEPEND="sys-devel/bison"
RDEPEND=""

PATCHES=(
)

src_compile() {
	emake bootstrap
}
