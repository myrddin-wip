# Copyright 1999-2019 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit git-r3 myrddin-wip

DESCRIPTION="A semi-implementation of fmt(1)"
HOMEPAGE="https://repo.or.cz/efmt.git"
EGIT_REPO_URI="https://repo.or.cz/efmt.git"

if [[ ${PV} == 9999 ]]
then
	:
else
	EGIT_COMMIT="v${PV}"
fi

LICENSE="ISC"
SLOT="0"
KEYWORDS="~amd64"
