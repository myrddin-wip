# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit git-r3 myrddin-wip

DESCRIPTION="A no frills delta debugger written in myrddin."
HOMEPAGE="http://acha.ninja"
EGIT_REPO_URI="https://github.com/andrewchambers/ddmin"
EGIT_COMMIT="5de5a4c7218e658e67c63318e6b50d1f799b53cb"

LICENSE="ISC"
SLOT="0"
KEYWORDS="~amd64"

PATCHES=(
	"${FILESDIR}/ddmin-5de5a4c7218e-Replace-bio-return-codes-for-mc.git.patch"
)
