# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit git-r3 myrddin-wip

DESCRIPTION="Irc.myr is a simple IRC client for IRC."
HOMEPAGE="https://eigenstate.org/software/ircmyr"
EGIT_REPO_URI="git://git.eigenstate.org/ori/irc.myr.git/"
EGIT_COMMIT="ba49d299fd4a587c41eb6dbadd5d64b1d0191410"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64"
